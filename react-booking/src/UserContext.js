import React from 'react';

const UserContext = React.createContext();

//Creates a context object
//a context object as the name states is a data type of an object that can be used to store information that can be shared to other components within the app

//The "Provider" component allows the other components to consume/use the context object and supply the necessary info needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;